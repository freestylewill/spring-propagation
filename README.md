# spring-propagation

#### 介绍
通过此项目可以针对Spring的七大传播机制(REQUIRED, SUPPORTS, MANDATORY, REQUIRES_NEW, NOT_SUPPORTED, NEVER, NESTED)进行详细测试 
希望能够帮助对于Spring事务有一定基础的同学，更加深刻的理解事务的传播机制

#### 软件架构
基于springboot、mybatis
实现对spring七大事务传播机制的测试

![输入图片说明](https://images.gitee.com/uploads/images/2019/1208/150318_0280d949_764599.png "微信截图_20191208150428.png")

说明：一般用得比较多的是 REQUIRED ， REQUIRES_NEW；REQUIRES_NEW 一般用在子方法需要单独事务。



