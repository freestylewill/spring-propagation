package com.veromca.transaction.service;

import com.veromca.transaction.common.BuildPO;
import com.veromca.transaction.po.SysUserPO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/12/09
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class TransactionManagerServiceTest {
    @Autowired
    private TransactionManagerService transactionManagerService;

    @Test
    public void testTransactionManager() {
        SysUserPO sysUser = BuildPO.provideSysUser();
        transactionManagerService.testTransactionManager(sysUser,BuildPO.provideEmps(sysUser.getUserName()));
    }
}