package com.veromca.transaction.service;

import com.veromca.transaction.RandomUtil;
import com.veromca.transaction.common.BuildPO;
import com.veromca.transaction.po.AccountPO;
import com.veromca.transaction.po.SysUserPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/12/08
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class PropagationAServiceTest {
    @Autowired
    private PropagationAService propagationAService;


    @Test
    public void insertSysUser() {
        SysUserPO sysUser = BuildPO.provideSysUser();
        List<AccountPO> accounts = BuildPO.provideEmps(sysUser.getUserName());
        propagationAService.insertSysUser(sysUser, accounts);
    }

    @Test
    public void insertSysUserTransaction() {
        SysUserPO sysUser = BuildPO.provideSysUser();
        List<AccountPO> accounts = BuildPO.provideEmps(sysUser.getUserName());
        propagationAService.insertSysUserTransaction(sysUser, accounts);
    }

    @Test
    public void insertSteps() {
        SysUserPO sysUser = BuildPO.provideSysUser();
        List<AccountPO> accounts = BuildPO.provideEmps(sysUser.getUserName());
        propagationAService.insertSteps(sysUser, accounts);
    }

    @Test
    public void transactionAll() {
        SysUserPO sysUser = BuildPO.provideSysUser();
        List<AccountPO> accounts = BuildPO.provideEmps(sysUser.getUserName());
        propagationAService.transactionAll(sysUser, accounts);
    }



}