package com.veromca.transaction.common;

import com.veromca.transaction.RandomUtil;
import com.veromca.transaction.po.AccountPO;
import com.veromca.transaction.po.SysUserPO;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/12/09
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
public class BuildPO {
    public static List<AccountPO> provideEmps(String userName) {
        List<AccountPO> accounts = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            AccountPO account = new AccountPO();
            account.setUserName(userName);
            account.setAccountNo(RandomUtil.idcard());
            accounts.add(account);
        }
        return accounts;
    }

    public static SysUserPO provideSysUser() {
        SysUserPO sysUser = new SysUserPO();
        sysUser.setUserName(RandomUtil.username());
        sysUser.setAge(RandomUtils.nextInt(18,60));
        return sysUser;
    }
}
