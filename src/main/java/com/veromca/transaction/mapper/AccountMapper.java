package com.veromca.transaction.mapper;

import com.veromca.transaction.po.AccountPO;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public interface AccountMapper extends Mapper<AccountPO> {
}
