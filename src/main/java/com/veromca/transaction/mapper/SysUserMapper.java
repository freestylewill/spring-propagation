package com.veromca.transaction.mapper;

import com.veromca.transaction.po.SysUserPO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public interface SysUserMapper extends Mapper<SysUserPO> , MySqlMapper<SysUserPO>{
    int save(SysUserPO userPO);
}
