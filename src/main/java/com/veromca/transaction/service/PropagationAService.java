package com.veromca.transaction.service;

import com.veromca.transaction.mapper.AccountMapper;
import com.veromca.transaction.mapper.SysUserMapper;
import com.veromca.transaction.po.AccountPO;
import com.veromca.transaction.po.SysUserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 事务传播机制类型测试服务A
 * @Author: liusongqing
 * @CreateDate: 2019/12/08
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@Service
public class PropagationAService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private  PropagationBService propagationBService;

    /**
     * 插入用户,这个方法会同时插入用户和账户
     * 由于无事务处理，添加了一个用户和一个账户，但后续的账户没有添加进去造成了脏数据
     */
    public void insertSysUser(SysUserPO sysUserPO, List<AccountPO> accountPOS){
        sysUserMapper.insert(sysUserPO);
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        for (AccountPO accountPO : accountPOS) {
            accountMapper.insert(accountPO);
            if(true) throw new IllegalArgumentException("出异常了");
        }
    }

    /**
     * 加上事务后，出异常回滚代码，一个部门和员工都没有添加进去，实现了幂等性
     * @param sysUser
     * @param accounts
     */
    @Transactional
    public void insertSysUserTransaction(SysUserPO sysUser, List<AccountPO> accounts){
        sysUserMapper.insert(sysUser);

        for (AccountPO account : accounts) {
            accountMapper.insert(account);
            if(true) throw new IllegalArgumentException("出异常了");
        }
    }

    /**
     * 分步操作，被调用的方法无事务，但此方法有事务，出异常回滚，无记录添加
     * @param sysUser
     * @param accounts
     */
    @Transactional
    public void insertSteps(SysUserPO sysUser, List<AccountPO> accounts){
        sysUserMapper.insert(sysUser);

        // insertAccountPOs 无事务，会加入当前事务
        propagationBService.insertAccounts(accounts);
    }

    /**
     * 测试嵌套事务
     * 可以修改调用方法来达到不同的效果
     */
    @Transactional
    public void transactionAll(SysUserPO sysUser, List<AccountPO> accounts){
        sysUserMapper.insert(sysUser);
        //propagationBService.insertAccountsTransactionRequired(accounts);
        //propagationBService.insertAccountsTransactionRequiredNew(accounts);
        //propagationBService.insertAccountsTransactionNotSupport(accounts);
        //propagationBService.insertAccountsTransactionNever(accounts);
        //propagationBService.insertAccountsTransactionMandatory(accounts);
        //propagationBService.insertAccountsTransactionSupport(accounts);
        try {
            propagationBService.insertAccountsTransactionNested(accounts);
        }catch (Exception e){
            e.printStackTrace();
        }
        if(true) throw new IllegalArgumentException("出异常了");
    }



}
