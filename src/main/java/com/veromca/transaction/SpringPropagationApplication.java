package com.veromca.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
//开启事务支持
@EnableTransactionManagement
@MapperScan(basePackages = "com.veromca.transaction.mapper")
public class SpringPropagationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPropagationApplication.class, args);
    }

}
